package psso.projekt1.tree;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import psso.projekt1.visitor.EvaluateVisitor;
import psso.projekt1.visitor.InFixPrintVisitor;
import psso.projekt1.visitor.PostFixPrintVisitor;
import psso.projekt1.visitor.PreFixPrintVisitor;
import psso.projekt1.visitor.Visitor;

public class TreeNodeTest {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	
	private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

	private final TreeNode rootNode1;
	
	private final TreeNode rootNode2;

	private final TreeNode rootNode3;
	
	public TreeNodeTest() {
		Parser parser = new Parser();
		rootNode1 = parser.parse(prepareDummyTreeString1());
		rootNode2 = parser.parse(prepareDummyTreeString2());
		rootNode3 = parser.parse(prepareDummyTreeString3());
	}
	
	@Before
	public void setUpStreams() {
	    System.setOut(new PrintStream(outContent));
	    System.setErr(new PrintStream(errContent));
	}

	@After
	public void cleanUpStreams() {
	    System.setOut(null);
	    System.setErr(null);
	}
	
	private String prepareDummyTreeString1() {
		return "1 + ( 2 - 3 * 4 ) / 5";
	}
	
	private String prepareDummyTreeString2() {
		return "( 1 + 2 ) * ( 3 + 4 )";
	}
	
	private String prepareDummyTreeString3() {
		return "1 / 3 + 5 % 3 - 1";
	}
	
	@Test
	public void inFixTest1() {
		Visitor visitor = new InFixPrintVisitor();
		rootNode1.accept(visitor);
		assertEquals("( 1 + ( ( 2 - ( 3 * 4 ) ) / 5 ) )".replace(" ",""), outContent.toString().replace(" ",""));
	}
	
	@Test
	public void inFixTest2() {
		Visitor visitor = new InFixPrintVisitor();
		rootNode2.accept(visitor);	
		assertEquals("( ( 1 + 2 ) * ( 3 + 4 ) )".replace(" ",""), outContent.toString().replace(" ",""));
	}
	
	@Test
	public void inFixTest3() {
		Visitor visitor = new InFixPrintVisitor();
		rootNode3.accept(visitor);	
		assertEquals("( ( 1 / 3 ) + ( ( 5 % 3 ) - 1 ) )".replace(" ",""), outContent.toString().replace(" ","")); 
	}
	
	@Test
	public void postFixTest1() {
		Visitor visitor = new PostFixPrintVisitor();
		rootNode1.accept(visitor);
		assertEquals("1 2 3 4 * - 5 / +".replace(" ",""), outContent.toString().replace(" ",""));
	}
	
	@Test
	public void postFixTest2() {
		Visitor visitor = new PostFixPrintVisitor();
		rootNode2.accept(visitor);
		assertEquals("1 2 + 3 4 + *".replace(" ",""), outContent.toString().replace(" ",""));
	}
	
	@Test
	public void postFixTest3() {
		Visitor visitor = new PostFixPrintVisitor();
		rootNode3.accept(visitor);
		assertEquals("1 3 / 5 3 % 1 - +".replace(" ",""), new String(outContent.toString()).replace(" ",""));
	}

	@Test
	public void preFixTest1() {
		Visitor visitor = new PreFixPrintVisitor();
		rootNode1.accept(visitor);
		assertEquals("+ 1 / - 2 * 3 4 5".replace(" ",""), outContent.toString().replace(" ",""));
	}
	
	@Test
	public void preFixTest2() {
		Visitor visitor = new PreFixPrintVisitor();
		rootNode2.accept(visitor);
		assertEquals("* + 1 2 + 3 4".replace(" ",""), outContent.toString().replace(" ",""));
	}
	
	@Test
	public void preFixTest3() {
		Visitor visitor = new PreFixPrintVisitor();
		rootNode3.accept(visitor);
		assertEquals("+ / 1 3 - % 5 3 1".replace(" ",""), outContent.toString().replace(" ",""));
	}

	@Test
	public void evaluateTest1() {
		EvaluateVisitor visitor = new EvaluateVisitor();
		rootNode1.accept(visitor);
		assertEquals(-1, visitor.getValue());
	}
	
	@Test
	public void evaluateTes2() {
		EvaluateVisitor visitor = new EvaluateVisitor();
		rootNode2.accept(visitor);
		assertEquals(21, visitor.getValue());
	}
	
	@Test
	public void evaluateTest3() {
		EvaluateVisitor visitor = new EvaluateVisitor();
		rootNode3.accept(visitor);
		assertEquals(1, visitor.getValue());
	}
}
