package psso.projekt1.tree;

import psso.projekt1.visitor.EvaluateVisitor;
import psso.projekt1.visitor.InFixPrintVisitor;
import psso.projekt1.visitor.PostFixPrintVisitor;
import psso.projekt1.visitor.PreFixPrintVisitor;

public class ParserTest {
	public static void main(String[] args) {
		Parser parser = new Parser();
		TreeNode rootNode = parser.parse("1 + ( 2 - 3 * 4 ) / 5"); // spaces are
																	// vital!!;

		System.out.println("InFixPrintVisitor");
		rootNode.accept(new InFixPrintVisitor());

		System.out.println("PreFixPrintVisitor");
		rootNode.accept(new PreFixPrintVisitor());

		System.out.println("PostFixPrintVisitor");
		rootNode.accept(new PostFixPrintVisitor());

		System.out.println("EvaluateVisitor");
		EvaluateVisitor evaluateVisitor = new EvaluateVisitor();
		rootNode.accept(evaluateVisitor);
		System.out.println("result: " + evaluateVisitor.getValue());
	}
}