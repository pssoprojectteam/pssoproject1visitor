package psso.projekt1.tree;

import psso.projekt1.visitor.Visitor;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class NumericNode implements TreeNode {

	private int value;
	
	@Override
	public void accept(Visitor visitor) {
		visitor.visitNumericNode(this);
	}
	
	@Override
	public String getLabel() {
		return String.valueOf(value);
	}
	
	public NumericNode(int value) {
		this.value = value;
	}
}