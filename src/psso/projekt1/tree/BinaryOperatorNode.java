package psso.projekt1.tree;

import psso.projekt1.visitor.Visitor;

public abstract class BinaryOperatorNode implements TreeNode {

	private TreeNode left;
	
	private TreeNode right;

	@Override
	public void accept(Visitor visitor) {
		visitor.visitBinaryOperatorNode(this);
	}
	
	public BinaryOperatorNode() {
		left = right = null;
	}
	
	public BinaryOperatorNode(TreeNode left, TreeNode right) {
		this.left  = left;
		this.right = right;
	}
	
	public abstract int compute(int a, int b);

	public TreeNode getLeft() {
		return left;
	}

	public TreeNode getRight() {
		return right;
	}
}