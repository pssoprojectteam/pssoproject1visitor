package psso.projekt1.tree;

import psso.projekt1.visitor.Visitor;

public interface TreeNode  {

	void accept(Visitor visitor);
	
	String getLabel();
}