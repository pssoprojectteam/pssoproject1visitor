package psso.projekt1.visitor;

import java.util.Stack;

import psso.projekt1.tree.BinaryOperatorNode;
import psso.projekt1.tree.NumericNode;
import psso.projekt1.tree.TreeNode;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class EvaluateVisitor implements Visitor {

	private Stack<Integer> stack = new Stack<>();
	
	public int getValue() {
		return stack.peek();
	}
	
	@Override
	public void visitBinaryOperatorNode(BinaryOperatorNode binaryOperatorNode) {
		String label = binaryOperatorNode.getLabel();
		TreeNode left = binaryOperatorNode.getLeft();
		TreeNode right = binaryOperatorNode.getRight();		
		
		left.accept(this); 
		right.accept(this); 
		stack.push(binaryOperatorNode.compute(stack.pop(), stack.pop()));
		
		
	}

	@Override
	public void visitNumericNode(NumericNode numericNode) {
		stack.push(Integer.valueOf(numericNode.getLabel()));
	}

}
