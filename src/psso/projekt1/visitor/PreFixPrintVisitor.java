package psso.projekt1.visitor;

import psso.projekt1.tree.BinaryOperatorNode;
import psso.projekt1.tree.NumericNode;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class PreFixPrintVisitor implements Visitor {

	@Override
	public void visitBinaryOperatorNode(BinaryOperatorNode binaryOperatorNode) {
		System.out.print(binaryOperatorNode.getLabel());
		binaryOperatorNode.getLeft().accept(this);	
		binaryOperatorNode.getRight().accept(this);
		
	}

	@Override
	public void visitNumericNode(NumericNode numericNode) {
		System.out.print(numericNode.getLabel());
	}

}
