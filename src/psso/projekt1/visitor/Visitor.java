package psso.projekt1.visitor;

import psso.projekt1.tree.BinaryOperatorNode;
import psso.projekt1.tree.NumericNode;

public interface Visitor {
	
	void visitBinaryOperatorNode(BinaryOperatorNode binaryOperatorNode);
	
	void visitNumericNode(NumericNode numericNode);
}
