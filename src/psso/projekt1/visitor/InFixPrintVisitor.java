package psso.projekt1.visitor;

import psso.projekt1.tree.BinaryOperatorNode;
import psso.projekt1.tree.NumericNode;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class InFixPrintVisitor implements Visitor {

	@Override
	public void visitBinaryOperatorNode(BinaryOperatorNode binaryOperatorNode) {
		System.out.print("(");
		binaryOperatorNode.getLeft().accept(this);
		System.out.print(binaryOperatorNode.getLabel());
		binaryOperatorNode.getRight().accept(this);
		System.out.print(")");
	}

	@Override
	public void visitNumericNode(NumericNode numericNode) {
		System.out.print(numericNode.getLabel());		
	}

}
