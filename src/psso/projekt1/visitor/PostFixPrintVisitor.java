package psso.projekt1.visitor;

import psso.projekt1.tree.BinaryOperatorNode;
import psso.projekt1.tree.NumericNode;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class PostFixPrintVisitor implements Visitor {

	@Override
	public void visitBinaryOperatorNode(BinaryOperatorNode binaryOperatorNode) {
		binaryOperatorNode.getLeft().accept(this);		
		binaryOperatorNode.getRight().accept(this);
		System.out.print(binaryOperatorNode.getLabel());
	}

	@Override
	public void visitNumericNode(NumericNode numericNode) {
		System.out.print(numericNode.getLabel());
	}

}
